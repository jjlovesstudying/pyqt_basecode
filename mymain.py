from PyQt5 import uic, QtCore, QtGui, QtWidgets
import sys
from mainWindow import Ui_MainWindow
from PyQt5.QtWidgets import QMainWindow
from PyQt5.QtCore import pyqtSlot


class MyWindow(QMainWindow):

    def __init__(self):
        super(MyWindow, self).__init__()

        # set up the UI
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        # Modifications here
        self.ui.myButton.clicked.connect(self.btn_pressed)


    @pyqtSlot()
    def btn_pressed(self):
        self.ui.myLabel.setText("Congratulations!")


if __name__ == "__main__":
    print("\nProgram started...\n")

    app = QtWidgets.QApplication(sys.argv)
    ui = MyWindow()
    #ui.showMaximized()
    ui.show()
    app.exec()

    print("\nBye...")