# Introduction

To create GUI (Graphical User Interface) application using **Qt Designer** and **PyQt5**.

### Integrating Qt Designer & Pycharm
In Pycharm, navigate to `File > Settings > Tools > External Tools > +`.

Make the follow settings according to your path:

- **Name:** `Qt Designer`
- **Program:** `C:\JJProgram\QtDesigner\designer.exe`
- **Working directory:** `$ProjectFileDir$` <- This is macro.

Do so for pyuic5 (converting `.ui` to `.py`)as well. 
- **Name:** `pyuic5`
- **Program:** `C:\JJProgram\Anaconda3\envs\env_sh\Scripts\pyuic5.exe`
- **Arguments:** `-x $FileName$ -o $FileNameWithoutExtension$.py`
- **Working directory:** `$ProjectFileDir$` <- This is macro.


### Files

**1. `mainWindow.ui`**

This is the file you save using **QtDesigner**.
> Please only use QtDesigner to edit and save. 

**2. `mainWindow.py`**

This is the file that will be created automatically using the command `pyuic5`. In **Pycharm**, right click on the `.ui` file, click `External Tools > pyuic5`. This will automatically generate the `.py` file.
> Do not make any code change here because all changes will be lost when running the command.

**3. `mymain.py`**

This should be the main python file that you should be running. All changes (e.g. `pyqtSlot()`) should be here.